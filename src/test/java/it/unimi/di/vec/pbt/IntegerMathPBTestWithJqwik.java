package it.unimi.di.vec.pbt;

import static it.unimi.di.vec.pbt.IntegerMathTest.EXAMPLES;
import static it.unimi.di.vec.pbt.IntegerMathTest.isBestResult;
import static org.assertj.core.api.Assertions.assertThat;

import net.jqwik.api.*;
import net.jqwik.api.constraints.Positive;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class IntegerMathPBTestWithJqwik {
  @Example
  @Tag("pbt")
  void testWithAssertJ() {
    assertThat(IntegerMath.sqrt2(9)).isEqualTo(3);
  }

  @Example
  @Tag("pbt")
  boolean testFortyNine() {
    return IntegerMath.sqrt2(49) == 7;
  }

  @Example
  @ParameterizedTest
  @ValueSource(ints = {1, 3, 7, 42})
  @Tag("pbt")
  boolean testForMany(int x) {
    return IntegerMath.sqrt2(x * x) == x;
  }

  @Property(tries = EXAMPLES, seed = "424242")
  @Tag("pbt")
  @Tag("slow")
  boolean sqrt2GivesBestResult(@ForAll @Positive int x) {
    return isBestResult(IntegerMath.sqrt2(x), x);
  }

  @Property(tries = EXAMPLES, seed = "424242")
  @Tag("pbt")
  @Tag("slow")
  @Disabled
  boolean sqrt1GivesBestResult(@ForAll @Positive int x) {
    return isBestResult(IntegerMath.sqrt1(x), x);
  }
}
