package it.unimi.di.vec.pbt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.quicktheories.QuickTheory.qt;
import static org.quicktheories.generators.SourceDSL.integers;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class IntegerMathTestPaolo {

  public static final int EXAMPLES = 1000;

  public static boolean isBestResult(int sqrtResult, int square) {

    int delta = IntegerMath.error(sqrtResult, square);

    return (delta < IntegerMath.error(sqrtResult + 1, square))
        && (delta < IntegerMath.error(sqrtResult - 1, square));
  }

  @Test
  void testSemplice_radiceQuadrata_9() {

    assertThat(IntegerMath.sqrt2(9)).isEqualTo(3);
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  void testPBT_radiceQuadrata_numeriMultipli() {

    qt().withFixedSeed(4242)
        .withExamples(EXAMPLES)
        .forAll(integers().allPositive())
        .checkAssert(i -> assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue());
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  void testPBT_radiceQuadrata_numeriQuadratiPerfetti() {

    qt().withFixedSeed(4242)
        .withExamples(10000)
        .forAll(
            integers()
                .between(
                    1,
                    10000)) // Necessario un limite superiore altrimenti il test fallisce (presunto
        // overflow).
        .checkAssert(i -> assertThat(isBestResult(IntegerMath.sqrt2(i * i), i * i)).isTrue());
  }
}
