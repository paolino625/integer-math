package it.unimi.di.vec.pbt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.quicktheories.QuickTheory.qt;
import static org.quicktheories.generators.SourceDSL.integers;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.IntStream;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class IntegerMathTest {

  public static final int EXAMPLES = 10000;
  public static final int MAX_ROOT = (int) Math.sqrt(Integer.MAX_VALUE);

  private static IntStream randomInts() {
    return new Random(424242).ints(EXAMPLES, 0, Integer.MAX_VALUE);
  }

  public static boolean isBestResult(int sqrtResult, int square) {
    int delta = IntegerMath.error(sqrtResult, square);
    return (delta < IntegerMath.error(sqrtResult + 1, square))
        && (delta < IntegerMath.error(sqrtResult - 1, square));
  }

  @Test
  void sqrt0OfNine() {
    assertThat(IntegerMath.sqrt0(9)).isEqualTo(3);
  }

  @ParameterizedTest
  @ValueSource(ints = {1, 7, 13, 42})
  void sqrt0OfASquare(int n) {
    assertThat(IntegerMath.sqrt0(n * n)).isEqualTo(n);
  }

  @Test
  void sqrt0OfFortyEight() {
    assertThat(IntegerMath.sqrt0(48)).isEqualTo(7);
  }

  @Test
  void sqrt0OfFortyTwo() {
    CompletableFuture<Integer> sqr = CompletableFuture.supplyAsync(() -> IntegerMath.sqrt0(42));
    assertThatThrownBy(() -> sqr.get(1, TimeUnit.SECONDS)).isInstanceOf(TimeoutException.class);
    assertThat(sqr).isNotCompleted();
  }

  @Test
  void sqrt1OfFortyTwo() {
    CompletableFuture<Integer> sqr = CompletableFuture.supplyAsync(() -> IntegerMath.sqrt1(42));
    assertThat(sqr).succeedsWithin(Duration.ofSeconds(1)).isEqualTo(6);
  }

  @ParameterizedTest
  @ValueSource(ints = {1 * 1, 3 * 3, 7 * 7, 13 * 13, 42 * 42, 48})
  void sqrt1SameAsSqrt0(int x) {
    assertThat(IntegerMath.sqrt1(x)).isEqualTo(IntegerMath.sqrt0(x));
  }

  @Test
  void sqrt1OfFortySeven() {
    assertThat(IntegerMath.sqrt1(47)).isNotEqualTo(7);
  }

  @Test
  void sqrt2OfFortySeven() {
    assertThat(IntegerMath.sqrt2(47)).isEqualTo(7);
  }

  @ParameterizedTest
  @ValueSource(ints = {1 * 1, 3 * 3, 7 * 7, 13 * 13, 42 * 42, 42, 48})
  void sqrt2SameAsSqrt1(int x) {
    assertThat(IntegerMath.sqrt2(x)).isEqualTo(IntegerMath.sqrt1(x));
  }

  @RepeatedTest(EXAMPLES)
  @Tag("slow")
  void sqrt2Many(RepetitionInfo rep) {
    int i = rep.getCurrentRepetition();
    assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue();
  }

  @RepeatedTest(EXAMPLES)
  @Tag("slow")
  void sqrt2ManyRandom() {
    int i = (int) (Math.random() * Integer.MAX_VALUE);
    assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue();
  }

  @ParameterizedTest
  @Tag("slow")
  @MethodSource("randomInts")
  void sqrt2ManyRandomRepeatable(int i) {
    assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue();
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  void sqrt2CorrectWithTimeout() {
    qt().withFixedSeed(4242)
        .withUnlimitedExamples()
        .withTestingTime(5, TimeUnit.SECONDS)
        .forAll(integers().allPositive())
        .checkAssert(i -> assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue());
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  void sqrt2CorrectInManyExamples() {
    qt().withFixedSeed(4242)
        .withExamples(EXAMPLES)
        .forAll(integers().allPositive())
        .checkAssert(i -> assertThat(isBestResult(IntegerMath.sqrt2(i), i)).isTrue());
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  @Disabled
  void sqrt1NotCorrectWithTimeout() {
    qt().withFixedSeed(4242)
        .withUnlimitedExamples()
        .withTestingTime(5, TimeUnit.SECONDS)
        .forAll(integers().allPositive())
        .checkAssert(i -> assertThat(isBestResult(IntegerMath.sqrt1(i), i)).isTrue());
  }

  @Test
  @Tag("slow")
  @Tag("pbt")
  void sqrt1Incorrect() {
    assertThatThrownBy(
            () ->
                qt().withFixedSeed(4242)
                    .withUnlimitedExamples()
                    .withTestingTime(5, TimeUnit.SECONDS)
                    .forAll(integers().allPositive())
                    .check(i -> isBestResult(IntegerMath.sqrt1(i), i)))
        .isInstanceOf(AssertionError.class);
  }
}
