package it.unimi.di.vec.pbt;

public class IntegerMath {
  private IntegerMath() {}

  public static final int MAX_ITERATIONS = 10000;

  public static int sqrt0(int arg) {
    int guess = 1;
    while (error(guess, arg) > 1) {
      int q = arg / guess;
      guess = (guess + q) / 2;
    }
    return guess;
  }

  public static int sqrt1(int arg) {
    int guess = 1;
    int count = 0;
    while (error(guess, arg) > 1) {
      int q = arg / guess;
      guess = (guess + q) / 2;
      count += 1;
      if (count > MAX_ITERATIONS) break;
    }
    return guess;
  }

  public static int sqrt2(int arg) {
    int guess = 1;
    int count = 0;
    while (error(guess, arg) > 1) {
      int q = arg / guess;
      guess = (guess + q) / 2;
      count += 1;
      if (count > MAX_ITERATIONS) break;
    }
    int delta = error(guess, arg);
    if (error(guess - 1, arg) < delta) return guess - 1;
    if (error(guess + 1, arg) < delta) return guess + 1;
    return guess;
  }

  public static int error(int guessRoot, int square) {
    return Math.abs(guessRoot * guessRoot - square);
  }
}
